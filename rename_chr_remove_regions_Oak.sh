zgrep -v "#" Quercus_robur-GCA_932294415.1-2023_10-genes.gff3 | sed '/^$/d' |
    sed 's/^12/chr12/' | sed 's/^11/chr11/'| sed 's/^10/chr10/' | sed 's/^9/chr09/' |
    sed 's/^8/chr08/' | sed 's/^7/chr07/' | sed 's/^6/chr06/' | sed 's/^5/chr05/' |
    sed 's/^4/chr04/' | sed 's/^3/chr03/' | sed 's/^2/chr02/' | sed 's/^1/chr01/' |
    awk '{if ($3 !~ "region"){print $0}}' > Quercus_robur-GCA_932294415.1-2023_10-genes.rename_filter.gff3
    
# renamed the sequences in the gff to match the path names in the gfa
# removed regions that correspond to an entire chromosome, otherwise they would be transfered like the other annotations
