# Graph annotations

## objectives

Add annotation (genes, TE) directly on the graph.

## Tool to test

https://forge.ird.fr/diade/dynadiv/grannot

## Tools to install to use grannot
**IMPORTANT NOTE from Nina Marthe about the annotation of PGGB graphs (tested on the oak graph):**
"I was able to run grannot with the -gaf option, but when I tried to transfer annotations to a genome with -ann it took far too long. This is a known problem and will be fixed, but for the moment grannot is not compatible with PGGB graphs. Even in GAF it's possible that there could be errors."

### Singularity
To install Singularity and use it:
https://forgemia.inra.fr/formationcalcul2023/formation-singularity

Installation => folder `TP1_install`

```bash
# get the GrAnnoT image:
git clone https://forgemia.inra.fr/panannot/appli_containers/grannot.git

# Use the sandbox to DEBBUG:
    # creation of the sandbox (when buiding: argument "--sandbox target source")
sudo singularity build --sandbox  grannot_sandbox grannot_v1.0.2.sif
    # let's set the sandbox to be able edit it AND keep the modifications after exit
sudo singularity shell --writable grannot_sandbox

# Make your changes inside the sandbox (e.g. add `prints`)
    # for example install nano
apt install nano

# when all changes are done
exit

# to use the modified sandbox:
singularity exec grannot_sandbox grannot

# redo a sif image from the modified sandbox
    # but it's not a good parctice:
    # better to change the recipe according to your changes
sudo singularity build grannot_v1.0.2.sif grannot_sandbox

```

## Datasets

### oak datasets

- Quercus robur reference genome (source):
  - ID: dhQueRobu3.1
  - https://portal.darwintreeoflife.org/data/root/details/Quercus%20robur
  - https://www.ncbi.nlm.nih.gov/datasets/genome/GCF_932294415.1/
- Quercus robur gfa (pan1c workflow): https://web-genobioinfo.toulouse.inrae.fr/~amergez/Pangenomes/08Qr-v2a/
- Quercus robur annotations: https://projects.ensembl.org/darwin-tree-of-life/

### ztritici datasets (19 genomes)

- ztritici gfa pggb (pan1c workflow): https://bioinfo.bioger.inrae.fr/portal/data-browser/misc/panannot/ztritici/pggb/ztritici_19g_pggb.gfa.gz
- ztritici gfa Minigraph: https://bioinfo.bioger.inrae.fr/portal/data-browser/misc/panannot/ztritici/minigraph/ztritici_19g_minigraph.gfa.gz
- ztritici gfa Minigraph/Cactus: https://bioinfo.bioger.inrae.fr/portal/data-browser/misc/panannot/ztritici/mc/ztritici_19g_mc.gfa.gz
- ztritici gene annotations: https://bioinfo.bioger.inrae.fr/portal/data-browser/misc/panannot/ztritici/annotations/z.tritici.IP0323.reannot.gff3

### Pisum sativum (antitrypsic zone, 10 genomes)

- GFA and annotation for 5 genomes obtained with augustus.
    https://nextcloud.inrae.fr/s/p8zzj4jRLQNZMQn

## Tests of GrAnnoT

### ztritici

#### test add annotations in graph

```bash
# update sequence name in gff (same as in gfa)
sed '/^$/d' z.tritici.IP0323.reannot.gff3 | sed 's/^/IPO323./' > z.tritici.IP0323.reannot.fix.gff3

# add annotations on graph
singularity exec grannot_v1.0.2.sif grannot ztritici_19g_mc.gfa z.tritici.IP0323.reannot.fix.gff3 IPO323 -gaf -gff -v
```

results:

```txt
# annotated gff (seq = segment/ multiple segment same annotation)

>s2206695       GrAnnot gene    51      54      .       -       .       ID=ZtIPO323_125980;Name=ZtIPO323_125980;locus_tag=ZtIPO323_125980;;Rank_occurrence=1;Total_occurrences=66
>s2206696       GrAnnot gene    1       1       .       -       .       ID=ZtIPO323_125980;Name=ZtIPO323_125980;locus_tag=ZtIPO323_125980;;Rank_occurrence=2;Total_occurrences=66
>s2206698       GrAnnot gene    1       15      .       -       .       ID=ZtIPO323_125980;Name=ZtIPO323_125980;locus_tag=ZtIPO323_125980;;Rank_occurrence=3;Total_occurrences=66


# gaf (one line / multi seg / annot)
ZtIPO323_125980	1125	0	1125	-	>2206695>2206696>2206698>2206699>2206701>2206703>2206704>2206705>2206707>2206708>2206710>2206711>2206713>2206714>2206716>2206717>2206719>2206721>2206722>2206724>2206726>2206727>2206728>2206730>2206731>2206733>2206734>2206736>2206737>2206739>2206740>2206742>2206744>2206745>2206746>2206748>2206749>2206751>2206752>2206754>2206756>2206757>2206758>2206760>2206762>2206763>2206764>2206766>2206767>2206769>2206770>2206772>2206773>2206775>2206776>2206778>2206779>2206781>2206782>2206784>2206785>2206787>2206789>2206790>2206791>2206793	1186	50	1175	1125	1125	255
ZtIPO323_125980_1	1125	0	1125	-	>2206695>2206696>2206698>2206699>2206701>2206703>2206704>2206705>2206707>2206708>2206710>2206711>2206713>2206714>2206716>2206717>2206719>2206721>2206722>2206724>2206726>2206727>2206728>2206730>2206731>2206733>2206734>2206736>2206737>2206739>2206740>2206742>2206744>2206745>2206746>2206748>2206749>2206751>2206752>2206754>2206756>2206757>2206758>2206760>2206762>2206763>2206764>2206766>2206767>2206769>2206770>2206772>2206773>2206775>2206776>2206778>2206779>2206781>2206782>2206784>2206785>2206787>2206789>2206790>2206791>2206793	1186	50	1175	1125	1125	255

```

#### test pav / absence/presence matrix

```txt
singularity exec ../../containers/grannot/grannot_v1.0.2.sif grannot mc/ztritici_19g_mc.gfa annotations/z.tritici.IP0323.reannot.fix.gff3 IPO323 -ann -pav -v


Computing the segments coordinates on the genomes
     Extracting the S-lines from the GFA
     Computing the lengths of the segments: 100%|██████████████████████████████████████████████████████████| 7376092/7376092 [00:04<00:00, 1498807.94 segment/s]
     Extracting the W-lines or P-lines from the GFA
     Computing the segments coordinates: 100%|█████████████████████████████████████████████████████████████████████████████| 533/533 [01:01<00:00,  8.62 line/s]
Computing the intersection between the annotations and the graph segments
     Found 21 paths corresponding to the source genome
          Building the intersection for the path IPO323_IPO323.chr_14
          Building the intersection for the path IPO323_IPO323.chr_19
          Building the intersection for the path IPO323_IPO323.chr_2
          Building the intersection for the path IPO323_IPO323.chr_6
          Building the intersection for the path IPO323_IPO323.chr_21
          Building the intersection for the path IPO323_IPO323.chr_4
          Building the intersection for the path IPO323_IPO323.chr_11
          Building the intersection for the path IPO323_IPO323.chr_13
          Building the intersection for the path IPO323_IPO323.chr_1
          Building the intersection for the path IPO323_IPO323.chr_20
          Building the intersection for the path IPO323_IPO323.chr_10
          Building the intersection for the path IPO323_IPO323.chr_9
          Building the intersection for the path IPO323_IPO323.chr_8
          Building the intersection for the path IPO323_IPO323.chr_3
          Building the intersection for the path IPO323_IPO323.chr_12
          Building the intersection for the path IPO323_IPO323.chr_17
          Building the intersection for the path IPO323_IPO323.chr_16
          Building the intersection for the path IPO323_IPO323.chr_18
          Building the intersection for the path IPO323_IPO323.chr_7
          Building the intersection for the path IPO323_IPO323.chr_15
          Building the intersection for the path IPO323_IPO323.chr_5
Loading the intersection information: 100%|████████████████████████████████████████████████████████████████████| 9634360/9634360 [00:49<00:00, 195498.58 line/s]


Fetching all the genomes in the graph for the transfer: 100%|█████████████████████████████████████████████████████████████| 533/533 [00:00<00:00, 952.40 line/s]
     Genomes found : ['1A5', '1E4', '3D1', '3D7', 'Arg00', 'Aus01', 'CH95', 'CNR93', 'CRI10', 'I93', 'IR01_26b', 'IR01_48b', 'ISY92', 'KE94', 'OregS90', 'TN09', 'UR95', 'YEQ92']
Computing the lengths of the segments: 100%|███████████████████████████████████████████████████████████████| 7376092/7376092 [00:05<00:00, 1464348.24 segment/s]

1A5 transfer :
     Loading the walks for the genome 1A5
     Loading the segments coordinates for the path 1A5_1A5.chr_4
     Loading the segments coordinates for the path 1A5_1A5.chr_15
     Loading the segments coordinates for the path 1A5_1A5.chr_11
     Loading the segments coordinates for the path 1A5_1A5.chr_6
     Loading the segments coordinates for the path 1A5_1A5.chr_20
     Loading the segments coordinates for the path 1A5_1A5.chr_3
     Loading the segments coordinates for the path 1A5_1A5.chr_14
     Loading the segments coordinates for the path 1A5_1A5.chr_7
     Loading the segments coordinates for the path 1A5_1A5.chr_2
     Loading the segments coordinates for the path 1A5_1A5.chr_5
     Loading the segments coordinates for the path 1A5_1A5.chr_13
     Loading the segments coordinates for the path 1A5_1A5.chr_1
     Loading the segments coordinates for the path 1A5_1A5.chr_17
     Loading the segments coordinates for the path 1A5_1A5.chr_19
     Loading the segments coordinates for the path 1A5_1A5.chr_9
     Loading the segments coordinates for the path 1A5_1A5.chr_16
     Loading the segments coordinates for the path 1A5_1A5.chr_21
     Loading the segments coordinates for the path 1A5_1A5.chr_10
     Loading the segments coordinates for the path 1A5_1A5.chr_8
     Loading the segments coordinates for the path 1A5_1A5.chr_12
     Generating the 1A5 output
        Computing features paths in target genome:  18%|██████████▏                                              | 16429/92366 [00:01<00:04, 15293.07 feature/s]no occ found ??? >s3667540 1A5_1A5.chr_4 ZtIPO323_050490 copy_1
[['1A5.chr_4', 304759, 304886, '>', 21317]]
['>s3615554', '>s3615555', '>s3615557', '>s3615558', '>s3615559', '>s3615561', '>s3615563', '>s3615564', '>s3615565', '>s3615566', '>s3615568', '>s3615569', '>s3615570', '>s3615573', '>s3615574', '>s3615575', '>s3615578', '>s3615579', '>s3615580', '>s3615583', '>s3615584', '>s3615585', '>s3615587', '>s3615589', '>s3615594', '>s3615595', '>s3615597', '>s3615599', '>s3615600', '>s3615601', '>s3615603', '>s3615605', '>s3615606', '>s3615607', '>s3615608', '>s3615609', '>s3615614', '>s3615615', '>s3615618', '>s3615619', '>s3615622', '>s3615623', '>s3615624', '>s3615626', '>s3615627', '>s3615629', '>s3615630', '>s3615633', '>s3615634', '>s3615636', '>s3615637', '>s3615639', '>s3615640', '>s3615642', '>s3615643', '>s3615645', '>s3615647', '>s3615648', '>s3615649', '>s3615652', '>s3615653', '>s3615655', '>s3615657', '>s3615658', '>s3615659', '>s3615662', '>s3615663', '>s3615665', '>s3615667', '>s3615668', '>s3615669', '>s3615671', '>s3615672', '>s3615674', '>s3615675', '>s3615677', '>s3615678', '>s3615680', '>s3615681', '>s3615682', '>s3615684', '>s3615686', '>s3615689', '>s3615690', '>s3615691', '>s3615700', '>s3615701', '>s3615703', '>s3615705', '>s3615707', '>s3615708', '>s3615710', '>s3615711', '>s3615713', '>s3615714', '>s3615716', '>s3615717', '>s3615719', '>s3615720', '>s3615722', '>s3615723', '>s3615725', '>s3615726', '>s3615728', '>s3615729', '>s3615731', '>s3615732', '>s3615734', '>s3615735', '>s3615737', '>s3615738', '>s3615740', '>s3615741', '>s3615743', '>s3615744', '>s3615746', '>s3615747', '>s3615749', '>s3615750', '>s3615752', '>s3615753', '>s3615755', '>s3615756', '>s3615758', '>s3615759', '>s3615761', '>s3615762', '>s3615764', '>s3615765', '>s3615767', '>s3615768', '>s3615770', '>s3615771', '>s3615773', '>s3615774', '>s3615776', '>s3615777', '>s3615779', '>s3615780', '>s3615782', '>s3615783', '>s3615785', '>s3615786', '>s3615788', '>s3615789', '>s3615791', '>s3615792', '>s3615794', '>s3615795', '>s3615797', '>s3615798', '>s3615800', '>s3615801', '>s3615803', '>s3615804', '>s3615805', '>s3615808', '>s3615810', '>s3615811', '>s3615813', '>s3615814', '>s3615817', '>s3615818', '>s3615820', '>s3615821', '>s3615822', '>s3615824', '>s3615825', '>s3615827', '>s3615828', '>s3615830', '>s3615831', '>s3615836', '>s3615838', '>s3615839', '>s3615841', '>s3615842', '>s3615843', '>s3615845', '>s3615846', '>s3615847', '>s3615849', '>s3615851', '>s3615852', '>s3615854', '>s3615855', '>s3615857', '>s3615859', '>s3615860', '>s3615861', '>s3615862', '>s3615864', '>s3615867', '>s3615868', '>s3615871', '>s3615872']
[['1A5_1A5.chr_4', 'copy_1', ['>s3667540', '>s3667541', '>s3667543', '>s3667545', '>s3667546', '>s3667548', '>s3667549', '>s3667551', '>s3667552', '>s3667554', '>s3667555', '>s3667557', '>s3667558', '>s3667560', '>s3667562', '>s3667563', '>s3667565', '>s3667566', '>s3667568', '>s3667569', '>s3667570', '>s3667571', '>s3667573', '>s3667574', '>s3667576', '>s3667577', '>s3667579', '>s3667580', '>s3667582', '>s3667583', '>s3667585', '>s3667586', '>s3667588', '>s3667589', '>s3667591', '>s3667592', '>s3667594', '>s3667595', '>s3667596', '>s3667599', '>s3667600', '>s3667602', '>s3667604', '>s3667605', '>s3667607', '>s3667608', '>s3667610', '>s3667611', '>s3667613', '>s3667614', '>s3667616', '>s3667617', '>s3667618', '>s3667620', '>s3667621', '>s3667623', '>s3667624', '>s3667626', '>s3667627', '>s3667629', '>s3667630', '>s3667632', '>s3667633', '>s3667635', '>s3667636', '>s3667637', '>s3667638', '>s3667639', '>s3667641', '>s3667642', '>s3667644', '>s3667645', '>s3667646', '>s3667648', '>s3667650', '>s3667651', '>s3667653', '>s3667654', '>s3667656', '>s3667657', '>s3667659', '>s3667660', '>s3667662', '>s3667663', '>s3667665', '>s3667666', '>s3667668', '>s3667669', '>s3667671', '>s3667672', '>s3667674', '>s3667675', '>s3667677', '>s3667678', '>s3667680', '>s3667681', '>s3667683', '>s3667684', '>s3667686', '>s3667687', '>s3667689', '>s3667691', '>s3667692', '>s3667694', '>s3667695', '>s3667696', '>s3667698', '>s3667699', '>s3667701', '>s3667702', '>s3667704', '>s3667705', '>s3667706', '>s3667707', '>s3667708', '>s3674035', '>s3674036', '>s3674038', '>s3674039', '>s3674042', '>s3674043', '>s3674044', '>s3674045', '>s3674046', '>s3674048', '>s3674049', '>s3674051', '>s3674053', '>s3674054', '>s3674056', '>s3674057', '>s3674059', '>s3674060', '>s3674062', '>s3674063', '>s3674065', '>s3674067', '>s3674068', '>s3674069', '>s3674071', '>s3674072', '>s3674073', '>s3674074', '>s3674075', '>s3674077', '>s3674078', '>s3674080', '>s3674081', '>s3674083', '>s3674085', '>s3674086', '>s3674088', '>s3674089', '>s3674091', '>s3674092', '>s3674094', '>s3674096', '>s3674097', '>s3674099', '>s3674100', '>s3674102', '>s3674103', '>s3674105', '>s3674106', '>s3674108', '>s3674109', '>s3674111', '>s3674112', '>s3674114', '>s3674115', '>s3674117', '>s3674118', '>s3674120', '>s3674121', '>s3674123', '>s3674124', '>s3674125', '>s3674127', '>s3674128', '>s3674130', '>s3674132', '>s3674158', '>s3676100', '>s3676102', '>s3676103', '>s3676105', '>s3676106', '>s3676108', '>s3676109', '>s3676111', '>s3676112', '>s3676114', '>s3676115', '>s3676117', '>s3676118', '>s3676120']]]
        Computing features paths in target genome:  19%|██████████▊   

```

Problem !! no occ found ???

### Psativum

#### GAF or gff

```bash
grannot -v  ../data/all_seq_TI_PGGB_10.fa.gz.gfaffix.unchop.Ygs.view.gfa CAMEOR_TI.gff CAMEOR -gff -gaf
```

Work only if you GFF has "1" as reference (Second part of P field).

#### pav

```bash
grannot -v  ../data/all_seq_TI_PGGB_10.fa.gz.gfaffix.unchop.Ygs.view.gfa CAMEOR_TI.gff CAMEOR -ann -pav
```

Work well on this small exemple with only a few genes. If you change the reference you change the matrix :

CAMEOR :

```txt
|gene_id	|MGS0004|	ZW6|	PI180693|	CHAMPAGNE|
|---------|-------|----|---------|----------|
|g89|1|1|1|1|
|g98|1|1|1|1|
|g103|1|1|1|1|
|g107|1|1|0|0|
|g113|1|1|1|1|
|g114|1|1|1|0|
|g152|1|1|0|0|

MGS0004:

|gene_id	|CAMEOR|	ZW6|	PI180693|	CHAMPAGNE|
|---------|-------|----|---------|----------|
|g278|1|1|1|1|
|g279|1|1|1|1|
|g286|1|1|1|1|
|g290|1|1|0|0|
|g292|1|1|1|1|
|g293|1|1|1|0|
|g297|0|0|1|0|
```

I've checked what we obtain compared to phylogenetics and results seems coherent. We need to go further.

### oak (Quercus robur)

#### test add annotation

```bash
# update sequence name in gff (same as in gfa)
zgrep -v "#" Quercus_robur-GCA_932294415.1-2023_10-genes.gff3 | sed '/^$/d' |
    sed 's/^/dhQueRobu3-1./' > Quercus_robur-GCA_932294415.1-2023_10-genes.fixed.gff3

# add annotations on graph
imgdir="/home/ludo/Documents/01_professional/06_Projects_experiments/2019-2024_OakPanGenome/0.6_pangenome_networking/2024_SAPI_PANANNOT/00_containers/grannot"
gfadir="/run/media/ludo/KINGSTON/Backup/pangenoak/PGG_Mergez/08Qr-v2a"
genodir="/run/media/ludo/KINGSTON/Backup/pangenoak/SeqData_GenomeAssemblies/2022_DToL_Qrobur_dhQueRobu3.1"
singularity exec -B /run $imgdir/grannot_v1.0.2.sif grannot \
    $gfadir/pan1c.08Qr-v2a.gfa \
    $genodir/Quercus_robur-GCA_932294415.1-2023_10-genes.fixed.gff3 \
    dhQueRobu3-1 -gaf -gff -v -ht -sh 1
```
results:

```txt
singularity exec -B /run  $imgdir/grannot_v1.0.2.sif grannot     $gfadir/pan1c.08Qr-v2a.gfa  $genodir/Quercus_robur-GCA_932294415.1-2023_10-genes.fixed.gff3     dhQueRobu3-1 -gaf -gff -v -ht -sh 1
Computing the segments coordinates on the genomes
     Extracting the S-lines from the GFA
     Computing the lengths of the segments: 100%|███████████████| 57625452/57625452 [01:02<00:00, 927653.05 segment/s]
     Extracting the W-lines or P-lines from the GFA
     Converting paths in walks: 100%|████████████████████████████████████████████| 102/102 [02:59<00:00,  1.76s/ path]
     Computing the segments coordinates: 100%|███████████████████████████████████| 102/102 [02:16<00:00,  1.34s/ line]
Computing the intersection between the annotations and the graph segments
     Found 14 paths corresponding to the source genome haplotype 1
          Building the intersection for the path dhQueRobu3-1#1#chr01
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr01.bed has inconsistent naming convention for record:
chr01	0	694	>s578

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr01.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr01	0	694	>s578

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr01.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr01	0	694	>s578

          Building the intersection for the path dhQueRobu3-1#1#chr09
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr09.bed has inconsistent naming convention for record:
chr09	0	145	>s41645556

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr09.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr09	0	145	>s41645556

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr09.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr09	0	145	>s41645556

          Building the intersection for the path dhQueRobu3-1#1#chr05
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr05.bed has inconsistent naming convention for record:
chr05	0	14688	>s22522295

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr05.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr05	0	14688	>s22522295

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr05.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr05	0	14688	>s22522295

          Building the intersection for the path dhQueRobu3-1#1#chrCP
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chrCP.bed has inconsistent naming convention for record:
chrCP	0	161172	>s57625452

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chrCP.bed has inconsistent naming convention for record:
chrCP	0	161172	>s57625452

          Building the intersection for the path dhQueRobu3-1#1#chr04
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr04.bed has inconsistent naming convention for record:
chr04	0	391034	>s15966211

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr04.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr04	0	391034	>s15966211

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr04.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr04	0	391034	>s15966211

          Building the intersection for the path dhQueRobu3-1#1#chr12
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr12.bed has inconsistent naming convention for record:
chr12	0	6355	>s54801836

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr12.bed has inconsistent naming convention for record:
chr12	0	6355	>s54801836

          Building the intersection for the path dhQueRobu3-1#1#chr03
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr03.bed has inconsistent naming convention for record:
chr03	0	4	>s10742289

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr03.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr03	0	4	>s10742289

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr03.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr03	0	4	>s10742289

          Building the intersection for the path dhQueRobu3-1#1#chr06
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr06.bed has inconsistent naming convention for record:
chr06	0	9286	<s30329593

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr06.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr06	0	9286	<s30329593

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr06.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr06	0	9286	<s30329593

          Building the intersection for the path dhQueRobu3-1#1#chr02
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr02.bed has inconsistent naming convention for record:
chr02	0	124	>s3621882

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr02.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr02	0	124	>s3621882

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr02.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr02	0	124	>s3621882

          Building the intersection for the path dhQueRobu3-1#1#chrMT
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chrMT.bed has inconsistent naming convention for record:
chrMT	0	425	>s57625062

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chrMT.bed has inconsistent naming convention for record:
chrMT	0	425	>s57625062

          Building the intersection for the path dhQueRobu3-1#1#chr10
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr10.bed has inconsistent naming convention for record:
chr10	0	9956	>s45982277

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr10.bed has inconsistent naming convention for record:
chr10	0	9956	>s45982277

          Building the intersection for the path dhQueRobu3-1#1#chr07
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr07.bed has inconsistent naming convention for record:
chr07	0	116717	>s32858358

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr07.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr07	0	116717	>s32858358

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr07.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr07	0	116717	>s32858358

          Building the intersection for the path dhQueRobu3-1#1#chr11
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr11.bed has inconsistent naming convention for record:
chr11	0	135077	>s50365236

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr11.bed has inconsistent naming convention for record:
chr11	0	135077	>s50365236

          Building the intersection for the path dhQueRobu3-1#1#chr08
***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr08.bed has inconsistent naming convention for record:
chr08	0	3	>s36651768

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr08.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr08	0	3	>s36651768

***** WARNING: File /home/ludo/Documents/01_professional/06_Projects_experiments/0.1_proposals/2024_SAPI_PANANNOT/seg_coord/dhQueRobu3-1#1#chr08.bed has a record where naming convention (leading zero) is inconsistent with other files:
chr08	0	3	>s36651768

Loading the intersection information: 0 line [00:00, ? line/s]


Generation of the graph's gff: 0 segment [00:00, ? segment/s]
Generation of the graph's gaf: 0 feature [00:00, ? feature/s]

```

**For the moment, the result files are empty. The problem is due to the incomplete correspondance between the path names in the PGGB gfa file and the GFF.**

```txt
-rw-r--r-- 1 ludo ludo   0  4 juil. 14:21 intersect
-rw-r--r-- 1 ludo ludo   0  4 juil. 14:22 pan1c.08Qr-v2a.gaf
-rw-r--r-- 1 ludo ludo   0  4 juil. 14:22 pan1c.08Qr-v2a.gff
```

See script `rename_chr_remove_regions_Oak.sh` for a better correction of the GFF chromosome names.

**IMPORTANT NOTE from Nina Marthe:**
"I was able to run grannot with the -gaf option, but when I tried to transfer annotations to a genome with -ann it took far too long. This is a known problem and will be fixed, but for the moment grannot is not compatible with PGGB graphs. Even in GAF it's possible that there could be errors."


# *Ab initio* annotation

## Tools to curate models

https://github.com/salzberg-lab/PSAURON
--> using ML

https://gitlab.com/PlantGenomicsLab/easel
--> Annotation and model selection using Random Forest
